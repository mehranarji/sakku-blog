    <footer class="main-footer">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 text-center text-md-right mb-5 mb-md-0">
                    <a href="<?= esc_url( home_url( './' ) ) ?>" class="footer-logo logo-sakku">
                        <picture>
                            <source media="(min-width: 992px)" srcset="<?= get_template_directory_uri() ?>/asset/image/logo-gray-3x.png">
                            <source media="(min-width: 576px)" srcset="<?= get_template_directory_uri() ?>/asset/image/logo-gray-2x.png">
                            <img src="<?= get_template_directory_uri() ?>/asset/image/logo-gray.png" alt="Sakku Blog">
                        </picture>
                    </a>
                    <a href="https://sakku.cloud/" class="footer-logo logo-fanap mr-md-5" target="_blank">
                        <picture>
                            <img src="<?= get_template_directory_uri() ?>/asset/image/fanap.png" alt="Fanap Blog">
                        </picture>
                    </a>
                </div>
                <div class="col-md-6 text-center text-md-left">
                    <p class="mb-1 footer-copyright"> تمامی حقوق متعلق به پدیدآورندگان <a href="https://sakku.cloud/">سکو</a> می‌باشد </p>
                    <p class="mb-0 footer-designby">
                        <bdo dir="ltr"> Design by: <a href="https://wearecolorz.com/" target="_blank">Colorz Design Studio</a> </bdo>
                    </p>
                </div>
            </div>
        </div>
    </footer>

    <?php wp_footer() ?>
    
</body>

</html>