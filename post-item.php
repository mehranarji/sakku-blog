<?php
require_once "inc/post.php";
$post = new Post();
?>

<article class="post-item">
    <div class="post-img">
        <img src="<?= $post->thumbnail() ?>" alt="<?= $post->title() ?>">
    </div>
        <header class="post-header">
            <div class="post-header-item post-author">
                <img src="<?= $post->avatar() ?>" alt="" class="d-inline-block align-middle">
                <a href="<?= $post->authorLink() ?>" class="post-author-name"><?= $post->author() ?></a>
            </div>

            <div class="post-header-item post-date"><time datetime=""><?= $post->date() ?></time></div>

            <?php $cats = $post->categories() ?>
            <?php if ( !empty($cats) ) : ?>
            <div class="post-header-item post-category">
                <?php foreach ($cats as $key => $cat) : ?>
                    <a href="<?= get_category_link( $cat ) ?>"><?= $cat->name ?></a>
                <?php endforeach ?>
            </div>
            <?php endif ?>
        </header>
        <a href="<?= $post->permalink() ?>"><h2 class="post-title"><?= $post->title() ?></h2></a>
        <p class="post-excerpt"><?= $post->excerpt() ?></p>
</article>