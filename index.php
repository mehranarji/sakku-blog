<?php \get_header() ?>

<?php
global $wp_query;
$count = $wp_query->found_posts;
?>

<main>
    <div class="container">
            
        <?php if( is_category() ) : ?>
        <header class="archive-header">
            <h2 class="archive-title mb-0"><?php single_cat_title()  ?></h2>
            <p class="archive-desc"> <?= $count ?> مقاله در این دسته‌بندی موجود است </p>
        </header>
        <hr class="line-dotted">
        <?php elseif( is_tag() ) : ?>
        <header class="archive-header">
            <h2 class="archive-title mb-0"><?php single_tag_title()  ?></h2>
            <p class="archive-desc"> <?= $count ?> مقاله با این برچسب موجود است </p>
        </header>
        <hr class="line-dotted">
        <?php elseif( is_author() ) : ?>
        <header class="author-header" style="background-image: url(<?= get_user_meta( get_the_author_ID(), 'cover', true ) ?>)">
            <div class="author-header-content">
                <div class="author-img">
                    <img src="<?= get_avatar_url(get_the_author_email(), ['size' => 50]) ?>" alt="">
                </div>
                <div class="author-detail">
                    <h2 class="author-name"><?php the_author() ?></h2>
                    <p class="author-desc"><?php the_author_meta( 'description' ) ?></p>
    
                    <div class="author-socials">
                        <?php
                        $twitter = get_the_author_meta( 'twitter' );
                        $facebook = get_the_author_meta( 'facebook' );
                        $instagram = get_the_author_meta( 'instagram' );
                        ?>
                        <?php if ( !empty( $twitter ) ) : ?>
                        <a href="<?= $twitter ?>">
                            <picture>
                                <source media="(min-width: 992px)" srcset="<?= get_template_directory_uri() ?>/asset/image/twitter-3x.png">
                                <source media="(min-width: 576px)" srcset="<?= get_template_directory_uri() ?>/asset/image/twitter-2x.png">
                                <img src="<?= get_template_directory_uri() ?>/asset/image/twitter.png" alt="Twitter logo">
                            </picture>
                        </a>
                        <?php endif ?>
                        <?php if ( !empty( $instagram ) ) : ?>
                        <a href="<?= $instagram ?>">
                            <picture>
                                <source media="(min-width: 992px)" srcset="<?= get_template_directory_uri() ?>/asset/image/instagram-3x.png">
                                <source media="(min-width: 576px)" srcset="<?= get_template_directory_uri() ?>/asset/image/instagram-2x.png">
                                <img src="<?= get_template_directory_uri() ?>/asset/image/instagram.png" alt="Instagram logo">
                            </picture>
                        </a>
                        <?php endif ?>
                        <?php if ( !empty( $facebook ) ) : ?>
                        <a href="<?= $facebook ?>">
                            <picture>
                                <source media="(min-width: 992px)" srcset="<?= get_template_directory_uri() ?>/asset/image/facebook-3x.png">
                                <source media="(min-width: 576px)" srcset="<?= get_template_directory_uri() ?>/asset/image/facebook-2x.png">
                                <img src="<?= get_template_directory_uri() ?>/asset/image/facebook.png" alt="Facebook logo">
                            </picture>
                        </a>
                        <?php endif ?>
                    </div>

                    <p class="author-posts"> <?php the_author() ?> <?= $count ?> مقاله در بلاگ سکو نوشته است </p>
                </div>
            </div>
        </header>
        <hr class="line-dotted">
        <?php elseif( is_search() ) : ?>
        <header class="archive-header">
            <h2 class="archive-title mb-0">نتایج جستجو برای:‌ "<?= get_search_query()  ?>"</h2>
            <p class="archive-desc"> <?= $count ?> مقاله در این دسته‌بندی موجود است </p>
        </header>
        <hr class="line-dotted">
        <?php endif ?>
        
        
        <div class="post-list">
            <?php if ( have_posts() ) : ?>

            <div class="row">
            <?php while( have_posts() ) : the_post(); ?>
                <div class="col-md-6">
                    <?php get_template_part( 'post', 'item' ) ?>
                </div>
            <?php endwhile ?>
            </div>
            <?php else : ?>
                <!-- TODO: No post found -->
                <div class="jumbotron text-center">
                    <h1 class="display-4">متاسفیم!...</h1>
                    <p class="lead">مطلبی با چنین عنوانی در دسترس نداریم...</p>
                </div>
            <?php endif ?>
        </div>

        <?php Theme::pagination() ?>
    </div>
</main>

<?php get_template_part( 'section', 'subscribe' ) ?>

<?php get_footer() ?>
