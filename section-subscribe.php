<?php if ( shortcode_exists( 'email-subscribers-form' )) : ?>
<section class="subscription-section">
    <div class="container text-center">
        <h3 class="subscription-title"> اولین نفر باشید </h3>
        <p class="subscription-desc mb-5"> که از راه‌اندازی سکّو با خبر و از پیشنهاد‌‌های ویژه ما بهرمند می‌شوید. </p>

        <!-- <form action="" class="subscription-form">
            <div class="form-row">
                <div class="col-md-6 offset-md-2 mb-3 mb-md-0">
                    <label for="input-subscription-email" class="sr-only">پست الکترونیکی خود را وارد کنید</label>
                    <input type="text" id="input-subscription-email" class="form-control form-control-lg subscription-input" placeholder="پست الکترونیکی خود را وارد کنید">
                </div>
                <div class="col-md-2">
                    <button class="btn btn-block btn-lg btn-light subscription-btn">ثبت نام</button>
                </div>
            </div>
        </form> -->

        <?= do_shortcode( '[email-subscribers-form id="1"]' ) ?>
    </div>
</section>
<?php endif ?>
