<!DOCTYPE html>
<html <?php language_attributes() ?>>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php wp_title() ?></title>

    <?php wp_head() ?>
</head>

<body <?php body_class() ?>>
    <header class="main-header">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <a class="navbar-brand" href="<?= esc_url( home_url( '/' ) ) ?>">
                    <?php Theme::theLogo() ?>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <?php
                        Theme::mainNav();
                    ?>
                    <form class="form-inline my-2 my-lg-0 mr-auto main-header-search" action="<?= esc_url( home_url() ) ?>" method="GET">
                        <input class="form-control mr-sm-2" type="search" placeholder="جستجو" aria-label="جستجو" name="s" value="<?= get_search_query() ?>">
                        <button class="btn btn-outline-success my-2 my-sm-0 d-none" type="submit">جستجو</button>
                    </form>
                </div>
            </div>
        </nav>
    </header>