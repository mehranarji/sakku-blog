<?php get_header() ?>

<?php
require_once "inc/post.php";
$post = new Post();
?>

<main class="post-single">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <article>
                    <?php if ( $post->hasThumbnail() ) : ?>
                    <figure class="post-img">
                        <img src="<?= $post->thumbnail('full') ?>" alt="<?= $post->title() ?>">
                    </figure>
                    <?php endif ?>

                    <header class="post-header justify-content-around justify-content-lg-start">
                        <div class="post-header-item post-author">
                            <img src="<?= $post->avatar() ?>" alt="<?= $post->author() ?>" class="d-inline-block align-middle">
                            <a href="<?= $post->authorLink() ?>" class="post-author-name"><?= $post->author() ?></a>
                        </div>

                        <div class="post-header-item post-date"><time datetime=""><?= $post->date() ?></time></div>

                        <?php $cats = $post->categories() ?>
                        <?php if ( !empty($cats) ) : ?>
                        <div class="post-header-item post-category">
                            <?php foreach ($cats as $key => $cat) : ?>
                                <a href="<?= get_category_link( $cat ) ?>"><?= $cat->name ?></a>
                            <?php endforeach ?>
                        </div>
                        <?php endif ?>
                        <div class="post-time-to-read mr-lg-auto">
                            <img src="<?= get_template_directory_uri() ?>/asset/image/clock.svg" alt="">
                            <span>
                                <?= $post->timeToRead() ?> دقیقه زمان مناسب برای مطالعه این مقاله
                            </span>
                        </div>
                    </header>
                    <a href="#"><h2 class="post-title"><?= $post->title() ?></h2></a>

                    <div class="post-content"><?= $post->content() ?></div>
                </article>

                <?php 
                    $meta_key = PostMeta::GetMetaKey('authors');
                    $authors  = get_post_meta( $post->id(), $meta_key, false );
                ?>
                <footer class="post-footer">
                    <?php if ( count($authors) > 0 ) : ?>
                    <p class="text-center"><b> .مقاله‌ای که خواندید با تلاش همکاران زیر در تیم سکو گردآوری شده است </b></p>
                    <div class="post-authors">
                        <div class="row justify-content-center">
                            <?php foreach ($authors as $author_id) : ?>
                            <?php $author = new WP_User($author_id) ?>
                            <div class="col-md-2">
                                <a href="<?= get_author_posts_url( $author_id ) ?>" class="post-author">
                                    <div class="author-img">
                                        <img src="<?= get_avatar_url($author->user_email, ['size' => 50]); ?>" alt="<?= $author->display_name ?>">
                                    </div>
                                    <p class="author-name"><?= $author->display_name ?></p>
                                </a>
                            </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <?php endif ?>

                    <div class="post-share">
                        <div class="row align-items-center justify-content-center">
                            <div class="col-md-5">
                                <p class="mb-0 text-muted"> در شبکه‌های اجتماعی به اشتراک بگذارید </p>
                            </div>
                            <div class="col-md-3 text-left">
                                <?php
                                    $share = [
                                        "url"           => urlencode( $post->permalink() ),
                                        "title"         => urlencode( $post->title() ),
                                        "description"   => urlencode( $post->excerpt() )
                                    ]
                                ?>
                                <a href="<?= "https://www.linkedin.com/shareArticle?mini=true&url={$share['url']}&title={$share['title']}&summary={$share['description']}&source=blog" ?>" target="_blank" class="post-share-via">
                                    <img src="<?= get_template_directory_uri() ?>/asset/image/linkedin-2x.png" alt="">
                                </a>
                                <!-- <a href="#" class="post-share-via">
                                    <img src="<?= get_template_directory_uri() ?>/asset/image/instagram-2x.png" alt="">
                                </a> -->
                                <a href="<?= "https://twitter.com/intent/tweet?url={$share['url']}%2F&text={$share['title']}" ?>" target="_blank" class="post-share-via">
                                    <img src="<?= get_template_directory_uri() ?>/asset/image/twitter-2x.png" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </footer>

                <?php if ( is_singular( 'post' ) ) : ?>
                <div class="post-related">
                    <h3 class="section-title">مقالات مرتبط</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <?php
                            $prev_post = new Post(get_previous_post());
                            if ( $prev_post->exist() ) :
                            ?>
                            <article class="post-item">
                                <div class="post-img">
                                    <img src="<?= $prev_post->thumbnail('medium') ?>" alt="<?= $prev_post->title() ?>">
                                </div>
                                <header class="post-header">
                                    <div class="post-header-item post-author">
                                        <img src="<?= $prev_post->avatar() ?>" alt="<?= $prev_post->title() ?>" class="d-inline-block align-middle">
                                        <a href="<?= $prev_post->authorLink() ?>" class="post-author-name"><?= $prev_post->author() ?></a>
                                    </div>
    
                                    <div class="post-header-item post-date"><time datetime=""><?= $prev_post->date() ?></time></div>
    
                                    <?php $cats = $prev_post->categories() ?>
                                    <?php if ( !empty($cats) ) : ?>
                                    <div class="post-header-item post-category">
                                        <?php foreach ($cats as $key => $cat) : ?>
                                            <a href="<?= get_category_link( $cat ) ?>"><?= $cat->name ?></a>
                                        <?php endforeach ?>
                                    </div>
                                    <?php endif ?>
                                </header>
                                <a href="<?= $prev_post->permalink() ?>"><h2 class="post-title"><?= $prev_post->title() ?></h2></a>
                            </article>
                            <?php endif ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            $next_post = new Post(get_next_post());
                            if ( $next_post->exist() ) :
                            ?>
                            <article class="post-item">
                                <div class="post-img">
                                    <img src="<?= $next_post->thumbnail() ?>" alt="<?= $next_post->title() ?>">
                                </div>
                                <header class="post-header">
                                    <div class="post-header-item post-author">
                                        <img src="<?= $next_post->avatar() ?>" alt="<?= $next_post->title() ?>" class="d-inline-block align-middle">
                                        <a href="<?= $next_post->authorLink() ?>" class="post-author-name"><?= $next_post->author() ?></a>
                                    </div>
    
                                    <div class="post-header-item post-date"><time datetime=""><?= $next_post->date() ?></time></div>
    
                                    <?php $cats = $next_post->categories() ?>
                                    <?php if ( !empty($cats) ) : ?>
                                    <div class="post-header-item post-category">
                                        <?php foreach ($cats as $key => $cat) : ?>
                                            <a href="<?= get_category_link( $cat ) ?>"><?= $cat->name ?></a>
                                        <?php endforeach ?>
                                    </div>
                                    <?php endif ?>
                                </header>
                                <a href="<?= $next_post->permalink() ?>"><h2 class="post-title"><?= $next_post->title() ?></h2></a>
                            </article>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
                <?php endif ?>
            </div>
        </div>
    </div>
</main>

<?php get_template_part( 'section', 'subscribe' ) ?>

<?php get_footer() ?>
