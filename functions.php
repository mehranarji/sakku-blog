<?php
require_once("inc/theme.php");
require_once("inc/user-meta.php");
require_once("inc/post-meta.php");

$theme = new Theme();
new UserMeta();
new PostMeta();
