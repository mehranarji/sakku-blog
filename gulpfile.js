const gulp = require('gulp');
const zip = require("gulp-zip");
const sass = require("gulp-sass");
const browser = require("browser-sync");
const sourcemaps = require("gulp-sourcemaps");
const imgmin = require("gulp-imagemin");
const sassVariables = require("gulp-sass-variables");
const del = require("del");

const package = require('./package.json');

const init = function (cb) {
  browser.init({
    server: './',
    open: false,
    port: 3000,
    // reloadDelay: 2000
  });

  cb();
}

const wordpress = function (cb) {
  browser.init({
    proxy: 'localhost/wordpress/sakku',
    open: false,
    port: 3000,
    // reloadDelay: 2000
  });

  cb();
}

const style = function (cb) {
  gulp.src('asset/style/src/style.scss')
    .pipe(sourcemaps.init())
    .pipe(sassVariables({
      $version: package.version
    }))
    .pipe(sass({
      outputStyle: 'compressed'
    }).on('error', sass.logError))
    .pipe(sourcemaps.write('./', {
      includeContent: false
    }))
    .pipe(gulp.dest('./'))
    .pipe(browser.stream({
      match: '**/*.css'
    }));

  cb();
}

const reload = function (cb) {
  browser.reload();

  cb();
};

const img = function (cb) {
  gulp.src('temp/images/**/*')
    .pipe(imgmin())
    .pipe(gulp.dest('temp/img/'));

  cb();
}

const watch = function (cb) {
  //   gulp.watch(['css/*.scss', 'css/sass/*.scss'], style);
  gulp.watch(['asset/style/src/**/*.scss'], style);
  gulp.watch(['asset/script/*.js'], reload);
  gulp.watch(['*.html'], reload);
  gulp.watch(['**/*.php', '!vendor/**'], reload);

  cb();
};

const remove = function (cb) {
  del([
    package.name + package.version + '.zip'
  ]);
    
  cb();
}

const compress = gulp.series(remove, function (cb) {
  var now = new Date();
  
  gulp.src(['**/*', '!node_modules/**', '!temp/**', '!*.html', 'asset/style/src/**'])
    .pipe(zip(package.name + package.version + '.zip'))
    .pipe(gulp.dest('./'));
  
  cb();
});

exports.default = gulp.series(init, watch);
exports.wordpress = gulp.series(wordpress, watch);
exports.style = style;
exports.img = img;
exports.zip = compress;