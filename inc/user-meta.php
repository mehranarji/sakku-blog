<?php

class UserMeta
{
    public function __construct()
    {
        \add_action('edit_user_profile', [$this, 'show']);
        \add_action('show_user_profile', [$this, 'show']);
        \add_action('personal_options_update', [$this, 'update']);
        \add_action('edit_user_profile_update', [$this, 'update']);
    }

    /**
     * @param $user
     */
    public function show($user)
    {
        ?>
            <h3>اطلاعات مربوط به پوسته</h3>
            <table class="form-table">
                <tr>
                    <th>
                        <label for="cover">کاور</label>
                    </th>
                    <td>
                        <input type="text"
                        class="regular-text"
                        name="cover"
                        value="<?php echo \esc_attr(\get_user_meta($user->ID, 'cover', true))?>">
                        <button class="button media-button" type="button">انتخاب از گالری</button>
                        <p class="description">
                            آدرس تصویر را وارد کنید یا از گالری یک تصویر مناسب انتخاب کنید.
                        </p>
                    </td>
                </tr>
            </table>
        <?php
}

    /**
     * @param $user_id
     */
    public function update($user_id)
    {
        // check that the current user have the capability to edit the $user_id
        if (!\current_user_can('edit_user', $user_id)) {
            return false;
        }

        // create/update user meta for the $user_id
        return \update_user_meta(
            $user_id,
            'cover',
            $_POST['cover']
        );
    }
}
