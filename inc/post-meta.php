<?php
class PostMeta
{
    /**
     * @var array
     */
    private $meta_list = [
        'authors' => [
            'label'    => 'نویسنده',
            'multiple' => 5,
            'type'     => 'users'
        ],
    ];

    /**
     * @var string
     */
    private static $slug = 'post-authors';

    /**
     * @var string
     */
    private static $title = 'نویسندگان';

    public function __construct()
    {
        \add_action('add_meta_boxes', [$this, 'AddMetaBox']);
        \add_action( 'save_post', [$this, 'SaveMeta'], 10, 2 );
    }

    public function AddMetaBox()
    {
        \add_meta_box($this->slug, $this->title, [$this, 'MetaBox'], null, 'normal', 'default');
    }

    public function MetaBox($post)
    {
        $users = get_users( [] );
        ?>
            <?php \wp_nonce_field($this->slug . '-metabox', $this->slug . '-nonce')?>

            <?php foreach ($this->meta_list as $id => $args): ?>
            <p>
                <label></label>
                <br>
                <?php if ( $args['type'] === 'users' ) : ?>
                    <?php if ( $args['multiple'] ) : ?>
                    <?php $meta_values = \get_post_meta( $post->ID, "{$this->slug}-{$id}", false ) ?>
                        <?php for ($i=0; $i < $args['multiple']; $i++) : ?>
                            <select name="<?php echo "{$this->slug}-{$id}[{$i}]"?>" style="margin-bottom: .5em;">
                                <option value="">-خالی-</option>
                                <?php foreach ($users as $user) : ?>
                                    <?php $item_index = array_search($user->ID, $meta_values) ?>
                                    <?php if ( $item_index === false ) :?>
                                        <option value="<?= $user->ID ?>"><?= $user->display_name ?></option>
                                    <?php else : ?>
                                        <?php array_splice($meta_values, $item_index, 1) ?>
                                        <option value="<?= $user->ID ?>" selected><?= $user->display_name ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
                            <br />
                        <?php endfor ?>
                    <?php endif ?>
                <?php endif ?>
            </p>
            <?php endforeach?>
        <?php
    }

    public function SaveMeta($post_id, $post)
    {
        /* Verify the nonce before proceeding. */
        if ( !isset( $_POST["{$this->slug}-nonce"] ) || !\wp_verify_nonce( $_POST["{$this->slug}-nonce"], $this->slug . '-metabox' ) )
        return $post_id;

        /* Get the post type object. */
        $post_type = \get_post_type_object( $post->post_type );

        /* Check if the current user has permission to edit the post. */
        if ( !\current_user_can( $post_type->cap->edit_post, $post_id ) )
        return $post_id;

        /* Get the posted data and sanitize it for use as an HTML class. */
        foreach ($this->meta_list as $id => $args) {
            /* Get the meta key. */
            $meta_key = $this->slug . '-' . $id;

            if ( $args['multiple'] === false ) {
                $new_meta_value = $_POST["{$this->slug}-{$id}"];
                
                /* Get the meta value of the custom field key. */
                $meta_value = \get_post_meta( $post_id, $meta_key, true );
    
                /* If a new meta value was added and there was no previous value, add it. */
                if ( $new_meta_value && '' == $meta_value )
                \add_post_meta( $post_id, $meta_key, $new_meta_value, true );
    
                /* If the new meta value does not match the old value, update it. */
                elseif ( $new_meta_value && $new_meta_value != $meta_value )
                \update_post_meta( $post_id, $meta_key, $new_meta_value );
    
                /* If there is no new meta value but an old value exists, delete it. */
                elseif ( '' == $new_meta_value && $meta_value )
                \delete_post_meta( $post_id, $meta_key, $meta_value );
            } else {
                // remove all old values
                \delete_post_meta( $post_id, $meta_key, null );
                
                $new_meta_values = array_unique($_POST["{$this->slug}-{$id}"]);
                foreach ($new_meta_values as $value) {
                    if ( $value !== null && trim($value) !== '' ) {
                        \add_post_meta( $post_id, $meta_key, $value, true );
                    }
                }
            }
        }
    }

    public static function GetMetaKey($meta_id)
    {
        $slug = self::$slug;
        return "{$slug}-{$meta_id}";
    }
}
