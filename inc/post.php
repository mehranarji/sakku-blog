<?php

class Post
{
    /**
     * @var mixed
     */
    private $date_format;

    /**
     * @var mixed
     */
    private $post;

    private static $words_per_minute = 300;

    /**
     * @param $post
     */
    public function __construct($post = null)
    {
        if ($post === null) {
            global $post;
        }

        if ( \is_object($post) && \get_class( $post ) === "WP_Post" ) {
            $this->post        = $post;
            $this->date_format = get_option('date_format');
        } else {
            $post = null;
        }
    }

    public function exist()
    {
        return $this->post != null;
    }
    
    public function author()
    {
        $author_id = get_post_field('post_author', $this->post);
        return get_author_name($author_id);
    }

    public function authorLink()
    {
        $author_id = get_post_field('post_author', $this->post);
        return get_author_posts_url($author_id);
    }

    /**
     * @param $size
     */
    public function avatar($size = 50)
    {
        $author_id    = get_post_field('post_author', $this->post);
        $author_email = get_the_author_meta('user_email', $author_id);
        return get_avatar_url($author_email, ['size' => $size]);
    }

    public function date()
    {
        return get_the_date($this->date_format, $this->post);
    }

    public function excerpt()
    {
        return get_the_excerpt($this->post);
    }

    public function permalink()
    {
        return get_the_permalink( $this->post, false );
    }

    /**
     * @param $size
     */
    public function thumbnail($size = 'full')
    {
        $thumbnail_id = get_post_thumbnail_id($this->post);
        $attachment   = wp_get_attachment_image_src($thumbnail_id, $size);
        return $attachment[0];
    }

    public function hasThumbnail()
    {
        return has_post_thumbnail( $this->post );
    }

    public function content()
    {
        return get_the_content( null, null, $this->post );
    }
    
    public function title()
    {
        return get_the_title($this->post);
    }

    public function categories()
    {
        $cats = get_the_category($this->post);
        return $cats;
    }

    public function timeToRead()
    {
        $content    = get_the_content(null, null, $this->post);
        $stripped   = strip_tags($content);
        $word_count = count(preg_split('~[\p{Z}\p{P}]+~u', $stripped, null, PREG_SPLIT_NO_EMPTY));

        $time = ceil($word_count / self::$words_per_minute);

        return $time;
    }

    public function id()
    {
        return $this->post->ID;
    }
}
