<?php

class Theme
{

    public function __construct()
    {
        add_action('init', [$this, 'menus']);
        add_filter('wp_title', [$this, 'title'], 10, 2);
        add_action('wp_enqueue_scripts', [$this, 'assets']);
        add_action('admin_enqueue_scripts', [$this, 'adminAssets']);
        add_action('after_setup_theme', [$this, 'setup']);
        add_action('widgets_init', [$this, 'widgets']);
    }

    public static function setup()
    {
        add_editor_style();
        add_theme_support('post-thumbnails');
        add_theme_support('automatic-feed-links');
        add_theme_support('html5', ['comment-list', 'comment-form', 'search-form', 'gallery', 'caption']);
        add_theme_support('custom-logo', [
            // 'height'      => 45,
            // 'width'       => 150,
            'flex-height' => true,
            'flex-width'  => true,
            'header-text' => ['site-title', 'site-description'],
        ]);
    }
    
    public static function assets()
    {
        $styles = [
            'app' => [
                'address' => get_stylesheet_uri(),
                'version' => self::themeVersion(),
            ],
        ];
        $scripts = [
            [
                'name'    => 'app',
                'src'     => get_template_directory_uri(), '/asset/script/app.js',
                'deps'    => ['jquery'],
                'version' => self::themeVersion(),
                'footer'  => true,
            ],
            [
                'name'    => 'jquery',
                'src'     => 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js',
                'deps'    => [],
                'version' => '3.4.1',
                'footer'  => true,
            ],
            [
                'name'    => 'bootstrap',
                'src'     => 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js',
                'deps'    => [],
                'version' => '4.4.1',
                'footer'  => true,
            ],
        ];

        if (is_singular()) {
            wp_enqueue_script("comment-reply");
        }

        foreach ($styles as $name => $attrs) {
            wp_enqueue_style($name, $attrs['address'], [], $attrs['version']);
        }

        foreach ($scripts as $script) {
            wp_enqueue_script($script["name"], $script["src"], $script["deps"], $script["version"], $script["footer"]);
        }
    }

    public static function adminAssets()
    {
        $styles = [
        ];
        $scripts = [
            [
                'name'    => 'app',
                'src'     => get_template_directory_uri() . '/asset/script/admin.js',
                'deps'    => [],
                'version' => self::themeVersion(),
                'footer'  => false,
            ]
        ];

        \wp_enqueue_media();

        foreach ($styles as $name => $attrs) {
            wp_enqueue_style($name, $attrs['address'], [], $attrs['version']);
        }

        foreach ($scripts as $script) {
            wp_enqueue_script($script["name"], $script["src"], $script["deps"], $script["version"], $script["footer"]);
        }
    }

    
    public static function menus()
    {
        register_nav_menu('main-nav', 'راهبری اصلی');
    }

    /**
     * @return mixed
     */
    public static function themeVersion()
    {
        $package_file = file_get_contents(__DIR__ . "/../package.json");
        $package      = json_decode($package_file);

        return $package->version;
    }

    /**
     * @return mixed
     */
    public static function title($title, $sep = "|")
    {
        if (is_feed()) {
            return $title;
        }

        global $page,
            $paged;

        // Add the blog name
        $title = get_bloginfo('name', 'display') . $title;

        // Add the blog description for the home/front page.
        $site_description = get_bloginfo('description', 'display');
        if ($site_description && (is_home() || is_front_page())) {
            $title .= " $sep $site_description";
        }

        // Add a page number if necessary:
        if (($paged >= 2 || $page >= 2) && !is_404()) {
            $title .= " $sep " . sprintf('Page %s', max($paged, $page));
        }

        return $title;
    }

    public static function widgets()
    {
        $args = [
            'name'          => 'فوتر',
            'id'            => 'footer-bar',
            'before_widget' => '<aside id="%s" class="widget %s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h3 class="widget-title %s">',
            'after_title'   => '</h3>',
        ];

        register_sidebar($args);
    }

    /**
     * @param array $classes
     */
    public static function theLogo($classes = [])
    {
        $custom_logo_id = get_theme_mod('custom_logo');
        $image          = wp_get_attachment_image_src($custom_logo_id, 'full');

        $image_alt = get_post_meta($custom_logo_id, '_wp_attachment_image_alt', true);
        if (empty($image_alt)) {
            $custom_logo_attr['alt'] = get_bloginfo('name', 'display');
        }

        $class = implode(' ', $classes);
        echo "<img class=\"site-logo lazy-load {$class}\" src=\"{$image[0]}\" alt=\"{$image_alt}\">";
    }

    public static function pagination($query = null)
    {
        $max_page = 0;
        if (isset($query)) {
            $max_page = $query->max_num_pages;
        } else {
            global $wp_query;
            $max_page = $wp_query->max_num_pages;
        }

        $big = 999999999;

        $links = paginate_links([
            'base'      => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
            'format'    => '?paged=%#%',
            'prev_next' => true,
            'prev_text' => 'قبلی',
            'next_text' => 'بعدی',
            'current'   => max(1, get_query_var('paged')),
            'total'     => $max_page,
            'type'      => 'array',
        ]);

        if ( !empty( $links )  ) {
            foreach ($links as $key => $link) {
                $links[$key] = str_replace('page-numbers', 'page-link', $link);
            }
            ?>
            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-center">
                    <?php foreach ($links as $key => $link) : ?>
                        <?php if ( strpos( $link, 'current' ) ) : ?>
                        <li class="page-item active">
                        <?php else : ?>
                        <li class="page-item">
                        <?php endif ?>
                            <?= $link ?>
                        </li>
                    <?php endforeach ?>
                </ul>
            </nav>
            <?php
        }

    }

    public static function mainNav()
    {
        $args = [
            'menu_class' => 'navbar-nav',
            'theme_location' => 'main-nav',
            'depth' => 1
        ];
        return wp_nav_menu( $args );
    }
}
